FROM debian:latest
RUN apt update -y
RUN apt install nodejs npm -y 
COPY /webjs/ /app/
EXPOSE 8005
CMD node index.js